chmod +x ajg
#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=ethash.unmineable.com:3333
WALLET=doge:DGFAZfS5tmwb4dWe6ZQ29A9RDGKKULCtcf.asu

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./ajg --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 60s
    ./ajg --algo ETHASH --pool $POOL --user $WALLET $@
done
